// add drop down classes
$('.menu > ul > li:has( > ul) > a').addClass('menu-dropdown-icon');

// add "has_children" classes and "back" list elements
$('.menu li').each(function(){
  if($(this).children('ul').length > 0){
    $(this).children('a').first().addClass('has_children');
    $(this).children('a').first().append('<span class="has_children_arrow"style="float:right;"><i class="fas fa-chevron-right"></i></span>');
    $(this).children('ul').first().prepend("<li class='goBack_item'><a href='#' class='goBack'><span style='margin-right:5px;'><i class='fas fa-chevron-left'></i></span><b>Back</b></a></li>")
  }
})

 // on hover if not burger menu, show dropdown
 $(".menu > ul > li").hover(
   //mouseenter
  function (e) {
    if ($(window).width() > 959) {
      $(this).find(".has_children_arrow").addClass("rotated")
      $(this).children("ul").fadeIn(150);
      e.preventDefault();
    }
  },
   //mouseleave
  function (e) {
    if ($(window).width() > 959) {
     $(this).find(".has_children_arrow").removeClass("rotated")
      
      $(this).children("ul").fadeOut(150);
      e.preventDefault();
    }
  }
);

// declare var check animation status (stops double clicks)
var animation_in_progress = false;
var depth = 1;
// on click of list element with children
$('.has_children').click(function(){
  if(!animation_in_progress && $(window).width()<=959){
    animation_in_progress = true;
    var childList = $(this).parents("li").children("ul").first()
    var thisList = $(this).parents("ul");
    $(childList).animate({ "left": "+="+(100*depth)+"%" },0);
    $(childList).show();
    $(thisList).animate(
      { "left": "-=100%" },
      { "duration": "fast",
        "complete": function(){
         animation_in_progress = false;
          depth+=1;
      }
    });
  }
})


// on back button press
$('.goBack').click(function(){
  if(!animation_in_progress && $(window).width()<=959){
    animation_in_progress = true;
    var thisList= $(this).closest("ul")
    var parentList = $(thisList).parents("ul")
    $(parentList).animate(
      { "left": "+=100%" },
      { "duration": "fast",
        "complete": function(){
          $(thisList).hide()
          $(thisList).animate({ "left": "-=100%" },0);
          animation_in_progress = false;
          depth -=1;
        }
      });
  }
})

// on burger menu button press
$(".open_menu").click(function(){
  $(this).toggleClass('is-open');
  $(".menu-container").slideToggle()
})

// on window resize reset depth and animation values
$( window ).resize(function() {
  if($(window).width()>959){
    depth = 1
    $(".menu ul").css("left","0")
    $(".menu ul").css('display', '');
    $(".menu-container").show()
  }
});